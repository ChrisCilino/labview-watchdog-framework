﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="CCSymbols" Type="Str"></Property>
	<Property Name="G3Tag:-:G3AutoDocPrinterReportComboTag:-:G3_Documentation Reports.lvlib:Project Report.lvclass::G3_Documentation Reports.lvlib:Confluence-JIRA.lvclass" Type="Str">187367045</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str">The TCS (Tool Chain Support) Watcdog is a framework that allows you to monitor a circumstance (like time) for a condition. If that condition is met, then an action is taken. The analogy is a watchdog (the observer) looking for a perpatrator (like time, or temperature or anything that can be tested). If the watchdog sees the perpatrator it barks (takes an action such as sending an email, text message, etc).</Property>
	<Property Name="PetranWayTag:-:PetranWayAutoDocPrinterReportComboTag:-:PetranWay_Documentation Reports.lvlib:Project Report.lvclass::PetranWay_Documentation Reports.lvlib:BitBucket Wiki Markdown.lvclass" Type="Str">C:\PetranWay\Wikis\LabVIEW Watchdog Framework\Developer Resources\APIs\Watchdog Project Documentation.md</Property>
	<Property Name="PetranWayTag:-:PetranWayAutoDocPrinterReportComboTag:-:PetranWay_Documentation Reports.lvlib:Project Report.lvclass::PetranWay_Documentation Reports.lvlib:Confluence.lvclass" Type="Str">237338712</Property>
	<Property Name="utf.calculate.project.code.coverage" Type="Bool">true</Property>
	<Property Name="utf.create.arraybrackets" Type="Str">[]</Property>
	<Property Name="utf.create.arraythreshold" Type="UInt">100</Property>
	<Property Name="utf.create.captureinputvalues" Type="Bool">true</Property>
	<Property Name="utf.create.captureoutputvalues" Type="Bool">true</Property>
	<Property Name="utf.create.codecoverage.flag" Type="Bool">false</Property>
	<Property Name="utf.create.codecoverage.value" Type="UInt">100</Property>
	<Property Name="utf.create.editor.flag" Type="Bool">false</Property>
	<Property Name="utf.create.editor.path" Type="Path"></Property>
	<Property Name="utf.create.nameseparator" Type="Str">/</Property>
	<Property Name="utf.create.precision" Type="UInt">6</Property>
	<Property Name="utf.create.repetitions" Type="UInt">1</Property>
	<Property Name="utf.create.testpath.flag" Type="Bool">false</Property>
	<Property Name="utf.create.testpath.path" Type="Path"></Property>
	<Property Name="utf.create.timeout.flag" Type="Bool">false</Property>
	<Property Name="utf.create.timeout.value" Type="UInt">0</Property>
	<Property Name="utf.create.type" Type="UInt">0</Property>
	<Property Name="utf.enable.RT.VI.server" Type="Bool">false</Property>
	<Property Name="utf.passwords" Type="Bin">&amp;Q#!!!!!!!)!%%!Q`````Q:4&gt;(*J&lt;G=!!":!1!!"`````Q!!#6"B=X.X&lt;X*E=Q!"!!%!!!!"!!!!#F652E&amp;-4&amp;.516)!!!!!</Property>
	<Property Name="utf.report.atml.create" Type="Bool">false</Property>
	<Property Name="utf.report.atml.path" Type="Path">ATML report.xml</Property>
	<Property Name="utf.report.atml.view" Type="Bool">false</Property>
	<Property Name="utf.report.details.errors" Type="Bool">false</Property>
	<Property Name="utf.report.details.failed" Type="Bool">false</Property>
	<Property Name="utf.report.details.passed" Type="Bool">false</Property>
	<Property Name="utf.report.errors" Type="Bool">true</Property>
	<Property Name="utf.report.failed" Type="Bool">true</Property>
	<Property Name="utf.report.html.create" Type="Bool">false</Property>
	<Property Name="utf.report.html.path" Type="Path">HTML report.html</Property>
	<Property Name="utf.report.html.view" Type="Bool">false</Property>
	<Property Name="utf.report.passed" Type="Bool">true</Property>
	<Property Name="utf.report.skipped" Type="Bool">true</Property>
	<Property Name="utf.report.sortby" Type="UInt">1</Property>
	<Property Name="utf.report.stylesheet.flag" Type="Bool">false</Property>
	<Property Name="utf.report.stylesheet.path" Type="Path"></Property>
	<Property Name="utf.report.summary" Type="Bool">true</Property>
	<Property Name="utf.report.txt.create" Type="Bool">false</Property>
	<Property Name="utf.report.txt.path" Type="Path">ASCII report.txt</Property>
	<Property Name="utf.report.txt.view" Type="Bool">false</Property>
	<Property Name="utf.run.changed.days" Type="UInt">1</Property>
	<Property Name="utf.run.changed.outdated" Type="Bool">false</Property>
	<Property Name="utf.run.changed.timestamp" Type="Bin">&amp;Q#!!!!!!!%!%%"5!!9*2'&amp;U:3^U;7VF!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	<Property Name="utf.run.days.flag" Type="Bool">false</Property>
	<Property Name="utf.run.includevicallers" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.flag" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.overwrite" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.path" Type="Path">test execution log.txt</Property>
	<Property Name="utf.run.modified.last.run.flag" Type="Bool">true</Property>
	<Property Name="utf.run.priority.flag" Type="Bool">false</Property>
	<Property Name="utf.run.priority.value" Type="UInt">5</Property>
	<Property Name="utf.run.statusfile.flag" Type="Bool">false</Property>
	<Property Name="utf.run.statusfile.path" Type="Path">test status log.txt</Property>
	<Property Name="utf.run.timestamp.flag" Type="Bool">false</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Build \ Install Stuff" Type="Folder">
			<Item Name="NIPKG" Type="Folder">
				<Item Name="Watchdog_BuildSteps.lvlib" Type="Library" URL="../../Custom Build Steps/NIPKG_LV/Watchdog_BuildSteps.lvlib"/>
				<Item Name="Watchdog_InstallSteps.lvlib" Type="Library" URL="../../Custom Build Steps/NIPKG_LV/Watchdog_InstallSteps.lvlib"/>
				<Item Name="VI Launcher.bat" Type="Document" URL="/&lt;userlib&gt;/_PetranWay/Custom Install Step Launcher/Source/VI Launcher.bat"/>
			</Item>
			<Item Name="VIP" Type="Folder">
				<Item Name="Post-Build Custom Action.vi" Type="VI" URL="../../Custom Build Steps/VIP/Post-Build Custom Action.vi"/>
				<Item Name="Post-Install Custom Action.vi" Type="VI" URL="../../Custom Build Steps/VIP/Post-Install Custom Action.vi"/>
				<Item Name="Post-Uninstall Custom Action.vi" Type="VI" URL="../../Custom Build Steps/VIP/Post-Uninstall Custom Action.vi"/>
				<Item Name="Pre-Build Custom Action.vi" Type="VI" URL="../../Custom Build Steps/VIP/Pre-Build Custom Action.vi"/>
				<Item Name="Pre-Install Custom Action.vi" Type="VI" URL="../../Custom Build Steps/VIP/Pre-Install Custom Action.vi"/>
				<Item Name="Pre-Uninstall Custom Action.vi" Type="VI" URL="../../Custom Build Steps/VIP/Pre-Uninstall Custom Action.vi"/>
			</Item>
			<Item Name="Build Spec" Type="Folder">
				<Item Name="NIPKG_LV" Type="Folder">
					<Item Name="SubSpecs" Type="Folder">
						<Item Name="SrcDist.ini" Type="Document" URL="../NIPKG_LV/SubSpecs/SrcDist.ini"/>
					</Item>
					<Item Name="Commit Behavior.ini" Type="Document" URL="../NIPKG_LV/Commit Behavior.ini"/>
					<Item Name="NIPKG_LV.ini" Type="Document" URL="../NIPKG_LV/NIPKG_LV.ini"/>
				</Item>
				<Item Name="VIP" Type="Folder">
					<Item Name="Commit Behavior.ini" Type="Document" URL="../VIP/Commit Behavior.ini"/>
					<Item Name="VIPackage.ini" Type="Document" URL="../VIP/VIPackage.ini"/>
				</Item>
				<Item Name="VIPackage.vipb" Type="Document" URL="../VIPackage.vipb"/>
				<Item Name="Software Module Definition.ini" Type="Document" URL="../Software Module Definition.ini"/>
				<Item Name="License.rtf" Type="Document" URL="../../License/License.rtf"/>
			</Item>
		</Item>
		<Item Name="Source" Type="Folder" URL="../../../Source">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="LabVIEWSMTPClient.lvlib" Type="Library" URL="/&lt;vilib&gt;/smtpClient/LabVIEWSMTPClient.lvlib"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Compare Two Paths.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Compare Two Paths.vi"/>
				<Item Name="NI_XML.lvlib" Type="Library" URL="/&lt;vilib&gt;/xml/NI_XML.lvlib"/>
				<Item Name="i3-json.lvlib" Type="Library" URL="/&lt;vilib&gt;/LVH/i3 JSON/i3-json.lvlib"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="PetranWay_SrcDist BldSpec Utils.lvlib" Type="Library" URL="/&lt;userlib&gt;/_PetranWay/SrcDist BldSpec Utils/Source/PetranWay_SrcDist BldSpec Utils.lvlib"/>
			</Item>
			<Item Name="DOMUserDefRef.dll" Type="Document" URL="DOMUserDefRef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="SrcDist" Type="Source Distribution">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{99DCA500-F991-405D-89C5-28D0F3B70CA8}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">SrcDist</Property>
				<Property Name="Bld_excludedDirectory[0]" Type="Path">vi.lib</Property>
				<Property Name="Bld_excludedDirectory[0].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[1]" Type="Path">resource/objmgr</Property>
				<Property Name="Bld_excludedDirectory[1].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[2]" Type="Path">/C/ProgramData/National Instruments/InstCache/17.0</Property>
				<Property Name="Bld_excludedDirectory[3]" Type="Path">/C/Users/labview/Documents/LabVIEW Data/2017(32-bit)/ExtraVILib</Property>
				<Property Name="Bld_excludedDirectory[4]" Type="Path">instr.lib</Property>
				<Property Name="Bld_excludedDirectory[4].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[5]" Type="Path">user.lib</Property>
				<Property Name="Bld_excludedDirectory[5].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectoryCount" Type="Int">6</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/builds/NI_AB_PROJECTNAME/SrcDist</Property>
				<Property Name="Bld_postActionVIID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/Watchdog_BuildSteps.lvlib/Post-Build Action_SrcDist.vi</Property>
				<Property Name="Bld_preActionVIID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/Watchdog_BuildSteps.lvlib/Pre-Build Action_SrcDist.vi</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{3CA134DB-F5EC-408A-8627-84EB5E9F7C49}</Property>
				<Property Name="Bld_userLogFile" Type="Path">/C/builds/NIPKG LV TCS_Watchdog/SrcDist/NIPKG LV TCS_Watchdog_SrcDist_log.txt</Property>
				<Property Name="Bld_version.build" Type="Int">22</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[0].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/SrcDist</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/SrcDist/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].destName" Type="Str">CustomInstallSteps</Property>
				<Property Name="Destination[2].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/SrcDist/CustomInstallSteps</Property>
				<Property Name="Destination[2].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[3].destName" Type="Str">NIPKGCustomInstallSteps</Property>
				<Property Name="Destination[3].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/SrcDist/CustomInstallSteps/NIPKG</Property>
				<Property Name="Destination[3].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[4].destName" Type="Str">License</Property>
				<Property Name="Destination[4].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/SrcDist/License</Property>
				<Property Name="Destination[4].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">5</Property>
				<Property Name="Source[0].itemID" Type="Str">{C5743392-F227-4FAF-A555-F9F7AA5F1631}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[1].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Source</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">Container</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/Watchdog_InstallSteps.lvlib</Property>
				<Property Name="Source[2].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Library</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/Watchdog_InstallSteps.lvlib/Post-Install All_NIPKG.vi</Property>
				<Property Name="Source[3].type" Type="Str">VI</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/Watchdog_InstallSteps.lvlib/Post-Install_NIPKG.vi</Property>
				<Property Name="Source[4].type" Type="Str">VI</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/Watchdog_InstallSteps.lvlib/Pre-Uninstall_NIPKG.vi</Property>
				<Property Name="Source[5].type" Type="Str">VI</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/VI Launcher.bat</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[7].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">3</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG</Property>
				<Property Name="Source[7].type" Type="Str">Container</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">4</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/Build \ Install Stuff/Build Spec/License.rtf</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">9</Property>
			</Item>
			<Item Name="NIPKG" Type="{E661DAE2-7517-431F-AC41-30807A3BDA38}">
				<Property Name="NIPKG_license" Type="Ref">/My Computer/Build \ Install Stuff/Build Spec/License.rtf</Property>
				<Property Name="NIPKG_releaseNotes" Type="Str"></Property>
				<Property Name="PKG_actions.Count" Type="Int">3</Property>
				<Property Name="PKG_actions[0].Arguments" Type="Str">2017 32 "Run VI=Post-Install_NIPKG.vi"</Property>
				<Property Name="PKG_actions[0].NIPKG.HideConsole" Type="Bool">false</Property>
				<Property Name="PKG_actions[0].NIPKG.IgnoreErrors" Type="Bool">false</Property>
				<Property Name="PKG_actions[0].NIPKG.Schedule" Type="Str">post</Property>
				<Property Name="PKG_actions[0].NIPKG.Step" Type="Str">install</Property>
				<Property Name="PKG_actions[0].NIPKG.Target.Child" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/VI Launcher.bat</Property>
				<Property Name="PKG_actions[0].NIPKG.Target.Destination" Type="Str">{CF8E20D1-06E1-4590-AA59-E084D90E3401}</Property>
				<Property Name="PKG_actions[0].NIPKG.Target.Source" Type="Ref">/My Computer/Build Specifications/SrcDist</Property>
				<Property Name="PKG_actions[0].NIPKG.Wait" Type="Bool">true</Property>
				<Property Name="PKG_actions[0].Type" Type="Str">NIPKG.Batch</Property>
				<Property Name="PKG_actions[1].Arguments" Type="Str">2017 32 "Run VI=Post-Install All_NIPKG.vi" "Mass Compile=False"</Property>
				<Property Name="PKG_actions[1].NIPKG.HideConsole" Type="Bool">false</Property>
				<Property Name="PKG_actions[1].NIPKG.IgnoreErrors" Type="Bool">false</Property>
				<Property Name="PKG_actions[1].NIPKG.Schedule" Type="Str">postall</Property>
				<Property Name="PKG_actions[1].NIPKG.Step" Type="Str">install</Property>
				<Property Name="PKG_actions[1].NIPKG.Target.Child" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/VI Launcher.bat</Property>
				<Property Name="PKG_actions[1].NIPKG.Target.Destination" Type="Str">{CF8E20D1-06E1-4590-AA59-E084D90E3401}</Property>
				<Property Name="PKG_actions[1].NIPKG.Target.Source" Type="Ref">/My Computer/Build Specifications/SrcDist</Property>
				<Property Name="PKG_actions[1].NIPKG.Wait" Type="Bool">true</Property>
				<Property Name="PKG_actions[1].Type" Type="Str">NIPKG.Batch</Property>
				<Property Name="PKG_actions[2].Arguments" Type="Str">2017 32 "Run VI=Pre-Uninstall_NIPKG.vi"</Property>
				<Property Name="PKG_actions[2].NIPKG.HideConsole" Type="Bool">false</Property>
				<Property Name="PKG_actions[2].NIPKG.IgnoreErrors" Type="Bool">false</Property>
				<Property Name="PKG_actions[2].NIPKG.Schedule" Type="Str">pre</Property>
				<Property Name="PKG_actions[2].NIPKG.Step" Type="Str">uninstall</Property>
				<Property Name="PKG_actions[2].NIPKG.Target.Child" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/VI Launcher.bat</Property>
				<Property Name="PKG_actions[2].NIPKG.Target.Destination" Type="Str">{CF8E20D1-06E1-4590-AA59-E084D90E3401}</Property>
				<Property Name="PKG_actions[2].NIPKG.Target.Source" Type="Ref">/My Computer/Build Specifications/SrcDist</Property>
				<Property Name="PKG_actions[2].NIPKG.Wait" Type="Bool">true</Property>
				<Property Name="PKG_actions[2].Type" Type="Str">NIPKG.Batch</Property>
				<Property Name="PKG_autoIncrementBuild" Type="Bool">false</Property>
				<Property Name="PKG_autoSelectDeps" Type="Bool">false</Property>
				<Property Name="PKG_buildNumber" Type="Int">38</Property>
				<Property Name="PKG_buildSpecName" Type="Str">NIPKG</Property>
				<Property Name="PKG_dependencies.Count" Type="Int">1</Property>
				<Property Name="PKG_dependencies[0].Enhanced" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[0].MaxVersion" Type="Str"></Property>
				<Property Name="PKG_dependencies[0].MaxVersionInclusive" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[0].MinVersion" Type="Str">1.1.0-25</Property>
				<Property Name="PKG_dependencies[0].MinVersionType" Type="Str">Inclusive</Property>
				<Property Name="PKG_dependencies[0].NIPKG.DisplayName" Type="Str">Custom Install Step Launcher - NIPKG - LV</Property>
				<Property Name="PKG_dependencies[0].Package.Name" Type="Str">petranway-custominstallsteplauncher-nipkg-lv</Property>
				<Property Name="PKG_dependencies[0].Package.Section" Type="Str">Infrastructure</Property>
				<Property Name="PKG_dependencies[0].Package.Synopsis" Type="Str">Launches the VI responsible for custom install \ unsinstall behavior.</Property>
				<Property Name="PKG_dependencies[0].Relationship" Type="Str">Required Dependency</Property>
				<Property Name="PKG_dependencies[0].Type" Type="Str">NIPKG</Property>
				<Property Name="PKG_description" Type="Str"></Property>
				<Property Name="PKG_destinations.Count" Type="Int">6</Property>
				<Property Name="PKG_destinations[0].ID" Type="Str">{016CC1B2-264C-470C-B093-E7CA1270212F}</Property>
				<Property Name="PKG_destinations[0].Subdir.Directory" Type="Str">user.lib</Property>
				<Property Name="PKG_destinations[0].Subdir.Parent" Type="Str">{2BB90BCF-9A1D-4DB7-B83D-56F563D1ACB2}</Property>
				<Property Name="PKG_destinations[0].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[1].ID" Type="Str">{2BB90BCF-9A1D-4DB7-B83D-56F563D1ACB2}</Property>
				<Property Name="PKG_destinations[1].Subdir.Directory" Type="Str">LabVIEW 2017</Property>
				<Property Name="PKG_destinations[1].Subdir.Parent" Type="Str">{6EE9AACE-AA65-470E-A576-5DCCB3FAF13A}</Property>
				<Property Name="PKG_destinations[1].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[2].ID" Type="Str">{6EE9AACE-AA65-470E-A576-5DCCB3FAF13A}</Property>
				<Property Name="PKG_destinations[2].Subdir.Directory" Type="Str">National Instruments</Property>
				<Property Name="PKG_destinations[2].Subdir.Parent" Type="Str">root_5</Property>
				<Property Name="PKG_destinations[2].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[3].ID" Type="Str">{97E511ED-1CDA-4207-B09C-834AB3D93DD9}</Property>
				<Property Name="PKG_destinations[3].Subdir.Directory" Type="Str">Watchdog</Property>
				<Property Name="PKG_destinations[3].Subdir.Parent" Type="Str">{BED7F222-5FE7-4320-A89B-9B4BE295E307}</Property>
				<Property Name="PKG_destinations[3].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[4].ID" Type="Str">{BED7F222-5FE7-4320-A89B-9B4BE295E307}</Property>
				<Property Name="PKG_destinations[4].Subdir.Directory" Type="Str">_PetranWay</Property>
				<Property Name="PKG_destinations[4].Subdir.Parent" Type="Str">{016CC1B2-264C-470C-B093-E7CA1270212F}</Property>
				<Property Name="PKG_destinations[4].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[5].ID" Type="Str">{CF8E20D1-06E1-4590-AA59-E084D90E3401}</Property>
				<Property Name="PKG_destinations[5].Subdir.Directory" Type="Str">Source</Property>
				<Property Name="PKG_destinations[5].Subdir.Parent" Type="Str">{97E511ED-1CDA-4207-B09C-834AB3D93DD9}</Property>
				<Property Name="PKG_destinations[5].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_displayName" Type="Str">Watchdog - NIPKG - LV</Property>
				<Property Name="PKG_displayVersion" Type="Str"></Property>
				<Property Name="PKG_homepage" Type="Str">https://bitbucket.org/ChrisCilino/labview-watchdog-framework</Property>
				<Property Name="PKG_lvrteTracking" Type="Bool">false</Property>
				<Property Name="PKG_maintainer" Type="Str">PetranWay &lt;christopher.cilino@petranway.com&gt;</Property>
				<Property Name="PKG_output" Type="Path">/C/builds/NI_AB_PROJECTNAME/NIPKG</Property>
				<Property Name="PKG_packageName" Type="Str">petranway-watchdog-nipkg-lv</Property>
				<Property Name="PKG_ProviderVersion" Type="Int">1810</Property>
				<Property Name="PKG_section" Type="Str">Infrastructure</Property>
				<Property Name="PKG_shortcuts.Count" Type="Int">0</Property>
				<Property Name="PKG_sources.Count" Type="Int">1</Property>
				<Property Name="PKG_sources[0].Destination" Type="Str">{CF8E20D1-06E1-4590-AA59-E084D90E3401}</Property>
				<Property Name="PKG_sources[0].ID" Type="Ref">/My Computer/Build Specifications/SrcDist</Property>
				<Property Name="PKG_sources[0].Type" Type="Str">Build</Property>
				<Property Name="PKG_sources[1].Destination" Type="Str">{6EE9AACE-AA65-470E-A576-5DCCB3FAF13A}</Property>
				<Property Name="PKG_sources[1].ID" Type="Ref"></Property>
				<Property Name="PKG_sources[1].Type" Type="Str">File</Property>
				<Property Name="PKG_synopsis" Type="Str">A generic watchdog framework for LabVIEW.</Property>
				<Property Name="PKG_version" Type="Str">1.0.0</Property>
			</Item>
		</Item>
	</Item>
</Project>
